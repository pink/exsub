#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#include <dbDefs.h>
#include <registryFunction.h>
#include <subRecord.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <epicsExit.h>

static long exsub(subRecord *precord)
{
  epicsExit(0);
  return 0;
}

/* Register these symbols for use by IOC code: */
epicsRegisterFunction(exsub);
